﻿using System;
using AutoMapper;
using Driver.WebSite;
using Driver.WebSite.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WebSiteTest
{
    [TestClass]
    public class PlatesResolverTests
    {
        [TestInitialize]
        public void SetUp()
        {
            AutoMapperConfig.CreateMappings();
        }

        [TestMethod]
        public void ShouldAddSpaceToDriverPlate()
        {
            var driver = new Driver.WebSite.Models.Driver
            {
                Plate = "KRA34654"
            };
            var driverViewModel = Mapper.Map<Driver.WebSite.Models.Driver, DriverInfoViewModel>(driver);

            Assert.AreEqual("KRA 34654", driverViewModel.Plate);
        }
    }
}
